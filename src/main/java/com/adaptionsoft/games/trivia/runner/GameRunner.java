
package com.adaptionsoft.games.trivia.runner;
import java.util.Random;

import com.adaptionsoft.games.uglytrivia.Game;
import com.adaptionsoft.games.uglytrivia.OutputConsole;


public class GameRunner {

	private static boolean keepPlaying;

	public static void main(String[] args) {
		OutputConsole outputConsole = new OutputConsole();
		Game aGame = new Game(outputConsole);

		aGame.addPlayer("Chet");
		aGame.addPlayer("Pat");
		aGame.addPlayer("Sue");
		
		Random rand = new Random();
	
		do {
			aGame.roll(rand.nextInt(5) + 1);
			if (rand.nextInt(9) == 7) {
				keepPlaying = aGame.wrongAnswer();
			} else {
				keepPlaying = aGame.wasCorrectlyAnswered();
			}
		} while (keepPlaying);

	}
}
