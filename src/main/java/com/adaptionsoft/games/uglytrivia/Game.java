package com.adaptionsoft.games.uglytrivia;

public class Game {
    int numberOfPlayers = 0;
    private PlayersParty playersParty = new PlayersParty();
    OutputEvent outputEvent;

    Board board = new Board();

    Player currentPlayer;

    QuestionsDeck popQuestions = new QuestionsDeck(QuestionCategory.POP);
    QuestionsDeck scienceQuestions = new QuestionsDeck(QuestionCategory.SCIENCE);
    QuestionsDeck sportsQuestions = new QuestionsDeck(QuestionCategory.SPORTS);
    QuestionsDeck rockQuestions = new QuestionsDeck(QuestionCategory.ROCK);

    public Game(OutputEvent outputEvent) {
        this.outputEvent = outputEvent;
    }

    public boolean addPlayer(String playerName) {
        playersParty.add(new Player(playerName, board.getFirstPlace()));

        numberOfPlayers++;
        outputEvent.outputEvent(playerName + " was added");
        outputEvent.outputEvent("They are player number " + playersParty.getNumberOfPlayers());
        return true;
    }

    public void roll(int roll) {
        currentPlayer = playersParty.getCurrentPlayer();
        outputEvent.outputEvent(currentPlayer + " is the current player");
        outputEvent.outputEvent("They have rolled a " + roll);

        if (currentPlayer.inPenaltyBox && isEven(roll)) {
            currentPlayer.isGettingOutOfPenaltyBox = false;
            outputEvent.outputEvent(currentPlayer + " is not getting out of the penalty box");
            return;
        }

        if (currentPlayer.inPenaltyBox && isOdd(roll)) {
            currentPlayer.isGettingOutOfPenaltyBox = true;
            outputEvent.outputEvent(currentPlayer + " is getting out of the penalty box");
        }

        movePlayer(currentPlayer, roll);
        outputEvent.outputEvent("The category is " + currentCategory());
        askQuestion();
    }

    private boolean isEven(int number) {
        return number % 2 == 0;
    }

	private boolean isOdd(int number) {
		return number % 2 != 0;
	}

	private void movePlayer(Player player, int roll) {
        player.place = board.getPlace(player.place.number + roll);

        outputEvent.outputEvent(player + "'s new location is " + player.place);
    }

    private void askQuestion() {
        if (currentCategory() == QuestionCategory.POP)
            outputEvent.outputEvent(popQuestions.questions.removeFirst());
        if (currentCategory() == QuestionCategory.SCIENCE)
            outputEvent.outputEvent(scienceQuestions.questions.removeFirst());
        if (currentCategory() == QuestionCategory.SPORTS)
            outputEvent.outputEvent(sportsQuestions.questions.removeFirst());
        if (currentCategory() == QuestionCategory.ROCK)
            outputEvent.outputEvent(rockQuestions.questions.removeFirst());
    }

    private QuestionCategory currentCategory() {
        return currentPlayer.place.category;
    }

    public boolean wasCorrectlyAnswered() {

        if (currentPlayer.inPenaltyBox && !currentPlayer.isGettingOutOfPenaltyBox) {
            playersParty.nextPlayer();
            return true;
        }

        if (currentPlayer.inPenaltyBox) {
            outputEvent.outputEvent("Answer was correct!!!!");
        } else {
            outputEvent.outputEvent("Answer was corrent!!!!");
        }

        addAGoldCoinToCurrentPlayer();
        boolean isGameContinuing = isGameContinuing();
        playersParty.nextPlayer();
        return isGameContinuing;
    }

    private void addAGoldCoinToCurrentPlayer() {
        currentPlayer.purse++;
        outputEvent.outputEvent(currentPlayer
                + " now has "
                + currentPlayer.purse
                + " Gold Coins.");
    }

    public boolean wrongAnswer() {
        outputEvent.outputEvent("Question was incorrectly answered");
        sendPlayerToPenaltyBox();

        playersParty.nextPlayer();
        return true;
    }

    private void sendPlayerToPenaltyBox() {
        outputEvent.outputEvent(currentPlayer + " was sent to the penalty box");
        currentPlayer.inPenaltyBox = true;
    }

    private boolean isGameContinuing() {
        return !(currentPlayer.purse == 6);
    }
}
