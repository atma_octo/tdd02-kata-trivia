package com.adaptionsoft.games.uglytrivia;

public class Place {
    final int number;
    final QuestionCategory category;

    public Place(int number, QuestionCategory category) {
        this.number = number;
        this.category = category;
    }

    @Override
    public String toString() {
        return String.valueOf(number);
    }
}
