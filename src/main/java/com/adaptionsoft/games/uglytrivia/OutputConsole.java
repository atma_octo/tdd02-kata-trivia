package com.adaptionsoft.games.uglytrivia;

public class OutputConsole implements OutputEvent{

    @Override
    public void outputEvent(Object event) {
        System.out.println(event);
    }

}
