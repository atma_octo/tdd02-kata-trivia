package com.adaptionsoft.games.uglytrivia;

public class Question {
    String name;

    public Question(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
