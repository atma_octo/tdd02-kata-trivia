package com.adaptionsoft.games.uglytrivia;

public class PlayersParty {
    Player[] players = new Player[6];
    int numberOfPlayers = 0;
    int currentPlayerIndex = 0;

    public void add(Player player) {
        players[numberOfPlayers] = player;
        numberOfPlayers++;
    }

    public int getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public Player getCurrentPlayer() {
        return players[currentPlayerIndex];
    }

    public void nextPlayer() {
        currentPlayerIndex++;
        if (currentPlayerIndex == getNumberOfPlayers()) currentPlayerIndex = 0;
    }
}
