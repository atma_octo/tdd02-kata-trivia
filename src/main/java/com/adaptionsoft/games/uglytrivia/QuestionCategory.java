package com.adaptionsoft.games.uglytrivia;

public enum QuestionCategory {
    POP("Pop"),
    SCIENCE("Science"),
    SPORTS("Sports"),
    ROCK("Rock");

    final private String value;

    QuestionCategory(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
