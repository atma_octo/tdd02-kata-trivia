package com.adaptionsoft.games.uglytrivia;

import java.util.List;

public class Board {
    final List<Place> places;

    public Board() {
        this.places = List.of(
                new Place(0, QuestionCategory.POP),
                new Place(1, QuestionCategory.SCIENCE),
                new Place(2, QuestionCategory.SPORTS),
                new Place(3, QuestionCategory.ROCK),
                new Place(4, QuestionCategory.POP),
                new Place(5, QuestionCategory.SCIENCE),
                new Place(6, QuestionCategory.SPORTS),
                new Place(7, QuestionCategory.ROCK),
                new Place(8, QuestionCategory.POP),
                new Place(9, QuestionCategory.SCIENCE),
                new Place(10, QuestionCategory.SPORTS),
                new Place(11, QuestionCategory.ROCK));
    }

    public Place getFirstPlace() {
        return places.get(0);
    }

    private int boardSize() {
        return this.places.size();
    }

    public Place getPlace(int number) {
        // On déteste cette méthode... (mais il n'y a que ça que l'on déteste !)
        if (outOfBoardBound(number)) return this.places.get(number - boardSize());

        return this.places.stream().filter(place -> place.number == number).findFirst().get();
    }

    private boolean outOfBoardBound(int number) {
        return number > boardSize() - 1;
    }
}
